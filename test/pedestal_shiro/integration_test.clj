(ns pedestal-shiro.integration-test
  (:import [org.apache.shiro SecurityUtils]
           [org.apache.shiro.util ThreadContext]
           [org.apache.shiro.mgt DefaultSecurityManager]
           [org.apache.shiro.authc AuthenticationException
            IncorrectCredentialsException])
  (:require [pedestal-shiro.core :as ps]
            [clojure.string]
            [io.pedestal.test :refer [response-for]]
            [pedestal-shiro.service :as service]
            [pedestal-shiro.server :as server]
            [pedestal-shiro.test :as pedestal-shiro-test]
            [io.pedestal.http :as bootstrap]
            [pocheshiro.core :as s]
            [ring.util.response :as ring-resp]
            [clojure.test :refer :all]))

(def session (atom (pedestal-shiro-test/servlet-session)))

(alter-var-root #'io.pedestal.test/test-servlet-request 
                (fn [f]
                  (fn [verb url & args]
                    (pedestal-shiro-test/request-wrapper (apply f verb url args)
                                                         @session))))

(use-fixtures :each (fn [f]
                      (reset! session (pedestal-shiro-test/servlet-session))
                      (f)))
(def service
  (::bootstrap/service-fn (bootstrap/create-servlet service/service)))

(deftest unprotected-page-is-accessible-without-login
  (let [res (response-for service :get "/public")]
    (is (= (:body res) "Public"))))

(deftest protected-page-when-accessed-without-authentication-redirects-to-login
  (let [res (response-for service :get "/secure")]
    (is (= (:status res) 302))
    (is (= (-> res :headers (get "Location")) "/access-denied?uri=%2Fsecure"))))

(deftest protected-page-is-accessible-after-login
  (let [res (response-for service :get "/do-login")]
    (is (= (:body res) "Login Successful"))
    (let [res (response-for service :get "/secure")]
      (is (= (:body res) "Secure")))))

(deftest pages-are-protected-with-roles
  (let [res (response-for service :get "/members-only")]
    (is (= (:status res) 302)))
  (let [res (response-for service :get "/do-login")]
    (let [res (response-for service :get "/members-only")]
      (is (= (:body res) "Members Only")))))
    
(deftest pages-are-protected-with-permissions
  (let [res (response-for service :get "/members-with-write")]
    (is (= (:status res) 302))
    (let [res (response-for service :get "/do-login")]
      (let [res (response-for service :get "/members-with-write")]
        (is (= (:body res) "Members With Write Only"))))))
