(ns pedestal-shiro.core
  (:import [org.apache.shiro.subject Subject]
           [org.apache.shiro.authc AuthenticationException]
           [org.apache.shiro.authz AuthorizationException]
           [org.apache.shiro.subject.support SubjectThreadState]
           [org.apache.shiro.web.servlet ShiroHttpServletRequest])
  (:require [pocheshiro.core :as shiro]
            [ring.util.response :as ring-resp]
            [io.pedestal.http.route :as route]
            [io.pedestal.http :as server]
            [io.pedestal.interceptor :as interceptor
             :refer [interceptor definterceptorfn]]))

(defn authenticated?
  []
  (.isAuthenticated (shiro/get-bound-subject)))

(def non-session-encoding-response #'shiro/non-session-encoding-response)
(def create-web-subject #'shiro/create-web-subject)

(defn associate-subject
  [security-manager-retriever]
  (fn [{req :request :as context}]
    (let [s-req  (:servlet-request req)
          s-ctxt (:servlet-context req)
          shiro-req (ShiroHttpServletRequest. s-req s-ctxt true)
          s-res (non-session-encoding-response (:servlet-response req))
          sec-mgr (security-manager-retriever req)
          subject (create-web-subject sec-mgr shiro-req s-res)
          shiro-state (SubjectThreadState. subject)
          req' (assoc req :servlet-response s-res :shiro-state shiro-state)]
      (.bind shiro-state)
            (assoc context :request req'))))

(defn cleanup-subject
  [{req :request :as context}]
  (if (:shiro-state req)
    (.clear ^SubjectThreadState (:shiro-state req)))
  context)

(defn save-request-and-redirect
  [{req :request res :response :as context} url]
  (let [{:keys [uri request-method query-string]} req
        url (if (and (= request-method :get) (some? uri))
              (if (some? query-string)
                (route/url-for url :params {:uri uri :q query-string})
                (route/url-for url :params {:uri uri}))
              (route/url-for url))]
    (assoc context :response (ring-resp/redirect url))))

(defn access-forbidden-catcher [login access-denied]
  (fn [context ex]
    (cond
      (instance? AuthenticationException ex)
      (save-request-and-redirect context login)
      (instance? AuthorizationException ex)
      (save-request-and-redirect context access-denied)
      :else
      (throw ex))))

(definterceptorfn security
  [{:keys [login-page access-denied-page security-manager-retriever]
    :or {login-page :login access-denied-page :access-denied}}]
  (interceptor :name ::security
               :enter (associate-subject security-manager-retriever)
               :leave cleanup-subject
               :error (access-forbidden-catcher login-page access-denied-page)))

(defn guard-with
  [check]
  (fn [{req :request :as context}]
    (shiro/enforce check context)))

(definterceptorfn guard
  [check]
  (interceptor :name ::guard
               :enter (guard-with check)))

