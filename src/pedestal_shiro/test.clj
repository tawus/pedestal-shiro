(ns pedestal-shiro.test
  (:import [javax.servlet.http HttpServletRequestWrapper]
           [org.apache.shiro.mgt DefaultSecurityManager]
           [org.apache.shiro SecurityUtils]
           [org.apache.shiro.util ThreadContext]))

(defn servlet-session []
  (let [attributes (atom {})
        valid (atom true)]
    (proxy [javax.servlet.http.HttpSession] []
      (setAttribute [k v] (swap! attributes assoc k v))
      (getAttribute [k] (@attributes k))
      (removeAttribute [k] (swap! attributes dissoc k))
      (invalidate []
        (reset! attributes {})
        (reset! valid false)))))

(defn request-wrapper [req session]
  (proxy [HttpServletRequestWrapper] [req]
    (getRemoteHost [] "localhost")
    (getCookies [] nil)
    (getSession
      ([create?] session)
      ([] session))))

(defmacro with-realm
  [realm & forms]
  `(do
     (SecurityUtils/setSecurityManager (DefaultSecurityManager. ~realm))
     ~@forms
     (SecurityUtils/setSecurityManager nil)
     (ThreadContext/remove)))
